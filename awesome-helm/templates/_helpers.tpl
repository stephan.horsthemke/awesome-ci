{{- define "awesome-helm.full_name" -}}
{{- printf "%s-%s" .Chart.Name .Release.Name | trunc 63 -}}
{{- end -}}


{{- define "awesome-helm.labels" }}
app: {{ .Values.environment }}
name: {{ .Release.Name }}
version: {{ .Chart.Version }}
{{- end }}