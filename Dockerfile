FROM alpine

RUN apk upgrade --no-cache \
  && apk add --no-cache \
    musl \
    build-base \
    python3 \
  && pip3 install --no-cache-dir --upgrade pip \
  && rm -rf /var/cache/* \
  && rm -rf /root/.cache/*

ADD src/requirements.txt /requirements.txt

RUN pip3 install -r /requirements.txt

ADD src /src
WORKDIR /src
RUN python3 manage.py

EXPOSE 80

CMD python3 manage.py runserver 0.0.0.0:80