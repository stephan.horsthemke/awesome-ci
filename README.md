# Awesome CI

This is a part of the devops course at the kth: 
https://github.com/KTH/devops-course/issues

1. Virtualenv
    * run: pip install virtualenv
    * run: mkdir your_virtual_env_folder_name
    * run: python3 -m venv your_virtual_env_folder_name
2. Activate your virtualenv
    * run: source your_virtual_env_folder_name/bin/activate
3. Install dependent packages
    * move to the location where 'requirement.txt' exists
    * run: pip install -r requirement.txt
4. Run unit test
    * move to the location where 'manage.py' exists
    * run: python manage.py test web
5. Run the webserver
    * $ python manage.py runserver IP:PORT
    * Go to http://IP:PORT/web
6. Build and Run test or webserver with docker
    * move to location of Dockerfile
    * $ docker build -t NAME:TAG .
    * $ docker run -it -p 8080:80 NAME:TAG
    * Go to http://localhost:8080/web




## Gitlab-CI
Gitlab-CI is an integrated continuous integration service of gitlab. The file ".gitlab-ci.yml" tells the [Runner](<https://gitlab.com/help/ci/runners/README.md>) of gitlab, what it is supposed to do. It is a pipeline which possibly build the project, tests it and maybe even deploys it (on staging?).
